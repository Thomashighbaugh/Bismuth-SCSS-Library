import React from "react";
import Layout from "../components/Layout";
import SEO from "../components/SEO";
import Components from "../components/List";


export default function Start() {
    return (
        <Layout>
            <SEO title="Home" />
            <div className="container-fluid">

                <Components />
            </div>
        </Layout>
    );
}
