import React from "react";
import Layout from "../components/Layout";
import SEO from "../components/SEO";
import About from "../components/About";


export default function Start() {
  return (
    <Layout>
      <SEO title="Home" />
        <div className="container-fluid">

        <About />
      </div>
    </Layout>
  );
}
