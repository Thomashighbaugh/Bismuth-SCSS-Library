import React from "react";
import { Link, StaticQuery, graphql } from "gatsby";

export const Label = ({ title, path }) => (
  <Link to={path} className="link margin-15-b" id="path">
    <div className="grow row">
      <div>
        <h2>{title}</h2>
      </div>
    </div>
  </Link>
);
export default function Projects() {
  return (
    <StaticQuery
      query={graphql`
        query Projects {
          allMarkdownRemark {
            edges {
              node {
                id
                frontmatter {
                  title
                  path
                }
              }
            }
          }
        }
      `}
      render={data => {
        let { edges } = data.allMarkdownRemark;
        return (
          <div>
            <div className="container-fluid">
              <h1>Generated Pages</h1>
              <hr className="hr" />
            </div>

            <div>
              {edges.map(item => (
                <Label {...item.node.frontmatter} />
              ))}
            </div>
          </div>
        );
      }}
    />
  );
}
