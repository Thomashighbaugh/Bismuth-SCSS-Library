import React, { Component } from 'react';

class Logo extends Component {
  render() {
    return (
      <div className="navbar-brand">
        <img src="../../../images/favicon-96x96.png" alt="site logo" />
      </div>
    );
  }
}
export default Logo;
