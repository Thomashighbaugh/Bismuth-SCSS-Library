import React, { Component } from "react";
import { Link } from "gatsby";
import Places from "../../data/Start";
class Header extends Component {
  render() {
    return (
      <header>
          <nav className="nav">

          <Link to="/" onlyActiveOnIndex className="nav-link">
            About
          </Link>
          <Link to="/components" onlyActiveOnIndex className="nav-link">
            Components
          </Link>

        </nav>
      </header>
    );
  }
}

export default Header;
