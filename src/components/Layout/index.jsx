import React from "react";
import PropTypes from "prop-types";
import { Footer } from "sld-component-library";
import Header from "../Header";

const Layout = ({ children }) => {
  return (
    <>
        <Header />
      <main>{children}</main>
      <Footer />
    </>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired
};

export default Layout;
